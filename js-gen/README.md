

## How it works
`docker-gen` reads a configuration file `versions.json` and generates a directory tree 
based on the versions and their variants as described in the config.

All files which do not match any pattern within the `.dockerignore` are copied into the target directory.

GOSS tests within the `tests`-directory are rendered into the target directory.

For every variant there is a `tags.txt` generated which contains a list of docker compatible tag-names. 

## Variables exposed to template

With configuration `versions.json`
```json
{
    "versions": {
        "0.1": {
            "variants": [
                "alpine3.15/default"
            ],
            "version": "0.1.0",
            "variables": {
                "foo": "bar"
            }
        }
    }
}
```

The following variables are exposed to the templates.
```json
{
    "image": {
        "name": "alpine",
        "version": "3.15",
        "variant": "default"
    },
    "variables": {
        "foo": "bar"
    },
    "service": {
        "version": "0.1.0"
    }
}
```
