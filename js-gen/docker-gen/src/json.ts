import fs, {PathLike} from "fs";

export default function parseJsonFile<T extends object>(jsonFile: PathLike): T {

    let parsedJsonFile;

    try {
        parsedJsonFile = JSON.parse(fs.readFileSync(jsonFile, 'utf-8').toString());
    } catch (error) {
        let failedReason = JSON.stringify(error);

        if (error instanceof SyntaxError) {
            failedReason = `${error.message}`;
        }

        throw new Error(`Failed to parse ${jsonFile}: ${failedReason}`);
    }

    const jsonFileContentType = typeof parsedJsonFile;

    if (jsonFileContentType !== 'object') {
        throw new Error(`Expected that "${jsonFile}" contains a JSON object. Got: ${jsonFileContentType}`);
    }

    return parsedJsonFile;
}
