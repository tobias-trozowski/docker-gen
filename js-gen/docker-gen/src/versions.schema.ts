import {Schema} from "@cfworker/json-schema";

const JsonSchema: Schema = {
    type: "object",
    required: ["versions"],
    properties: {
        defaultDebainSuite: {type: "string", minLength: 3},
        defaultAlpineSuite: {type: "string", minLength: 3},
        defaultDebianVersions: {
            type: "object",
            additionalProperties: {
                type: "string",
                enum: ["squeeze", "wheezy", "jessie", "stretch", "buster", "bullseye", "bookworm"]
            }
        },
        defaultAlpineVersions: {
            type: "object",
            additionalProperties: {type: "string", format: "regexp", pattern: "^[0-9]+\\.[0-9]+(\\.([0-9]+))?$"}
        },
        versionAliases: {type: "object", additionalProperties: {type: "string", minLength: 1}},
        versions: {
            type: "object",
            additionalProperties: {
                type: "object",
                required: ["variants", "version"],
                properties: {
                    variants: {type: "array", items: {type: "string"}},
                    variables: {type: "object", additionalProperties: true},
                    version: {
                        type: "string",
                        format: "regexp",
                        pattern: "^((([0-9]+)\\.([0-9]+)\\.([0-9]+)(?:-?([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?)(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?)$"
                    }
                }
            }
        }
    }
};

export default JsonSchema;
