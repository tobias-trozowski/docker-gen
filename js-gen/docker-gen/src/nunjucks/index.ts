import nunjucks from 'nunjucks';
import semver from "semver";

const nunjucksEnv = nunjucks.configure({
    autoescape: false,
    throwOnUndefined: true,
    noCache: true
});

nunjucksEnv.addFilter('semverMajor', (version: string) => (semver.coerce(version) ?? {major: undefined}).major);
nunjucksEnv.addFilter('semverMinor', (version: string) => (semver.coerce(version) ?? {minor: undefined}).minor);
nunjucksEnv.addFilter('semverPatch', (version: string) => (semver.coerce(version) ?? {patch: undefined}).patch);

export default nunjucksEnv;
