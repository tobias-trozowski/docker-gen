import shelljs from "shelljs";
import util from "util";
import {PathLike} from "fs";

export function shellExec(command: string) {
    const result = shelljs.exec(command);
    if (result.code !== 0) {
        throw Error(util.format('Command returned non-zero result: %s\n%s', command, result.stderr))
    }
    return result;
}

export function shellCopy(files: PathLike[], destination: PathLike) {
    if (files.length <= 0) {
        return;
    }

    const cmd = util.format('cp -a %s "%s"', files.join(' '), destination);
    return shellExec(cmd);
}

export default shelljs;
