import fs, {PathLike} from "fs-extra";
import {Logger} from "./logger";
import semver from 'semver';
import parseJsonFile from "./json.js";
import {Validator} from "@cfworker/json-schema";
import JsonSchema from './versions.schema.js'
import {ValidationResult} from "@cfworker/json-schema/src/types";
import {Configuration, VersionBuildInfo} from "./config.js";
import path from "path";
import nunjucks from "./nunjucks/index.js";
import util from "util";
import shell, {shellExec} from "./shell/index.js";
import parser from "gitignore-parser"

function fileExists(filepath: PathLike): boolean {
    try {
        fs.accessSync(filepath, fs.constants.F_OK);
        return true;
    } catch (e) {
        // ignore silently
    }
    return false;
}

function writeRenderedFile(file: string, res: string) {
    file = file.replace(/\.template$/, '');

    writeFile(file, res);
}

function writeFile(file: PathLike, content: string) {
    fs.writeFileSync(file, content);
}

export class App {
    static readonly DefaultAlpineSuite = '3.15';
    static readonly DefaultDebianSuite = 'bullseye'

    private readonly configurationFile: PathLike = 'versions.json';

    private readonly dockerfileTemplate: PathLike = 'Dockerfile.template';

    public constructor(private readonly logger: Logger) {

    }

    public sanityChecksPassing(): boolean {

        if (!fileExists(this.configurationFile)) {
            this.logger.error(`Unable to locate "${this.configurationFile}". Aborting.`)
        }

        if (!fileExists(this.dockerfileTemplate)) {
            this.logger.error(`Unable to locate "${this.dockerfileTemplate}". Aborting.`)
        }

        let schemaValidationResult = this.projectContainsValidVersionsConfiguration(this.configurationFile);

        if (schemaValidationResult.valid) {
            return true;
        }

        return this.failWithLoggedJsonSchemaValidationErrors(this.configurationFile, schemaValidationResult)
    }

    public run(): number {

        if (!this.sanityChecksPassing()) {
            return 1;
        }

        let returnResult = 0;
        const config: Configuration = parseJsonFile('versions.json');
        for (const [version, buildInfo] of Object.entries(config.versions)) {
            const rcVerson = version.replace(/-rc$/, '');

            fs.rmSync(version, {recursive: true, force: true});

            const rcFullVersion = (config.versions[rcVerson] ?? {version: undefined}).version;
            if (rcVerson != version && rcFullVersion) {
                const latestVersion = semver.rsort([buildInfo.version, rcFullVersion])[0];
                if (buildInfo.version.substring(0, rcFullVersion.length) == rcFullVersion || latestVersion == rcFullVersion) {
                    // "x.y.z-rc1" == x.y.z*
                    this.logger.warn(`RC version "${version}" already released. Skipping.`);
                    continue;
                }
            }

            for (const dir of buildInfo.variants) {
                fs.mkdirSync(`${version}/${dir}`, {recursive: true});

                try {
                    this.createVariant(version, dir, buildInfo);
                    this.createTest(version, dir);

                    fs.writeFileSync(
                        path.resolve(version, dir, 'tags.txt'),
                        this.getVariantAliases(version, dir, buildInfo.version, config).join(' ')
                    );

                    const dockerignore = parser.compile(fs.readFileSync('.dockerignore', 'utf8')),
                        filter = (file: string) => !/.*\.template$/ig.test(file) && dockerignore.maybe(file),
                        files = fs.readdirSync((process.env.PWD as PathLike))
                        .filter(filter);

                    for (const file of files) {
                        fs.copySync(file as string, path.resolve(version, dir, file), {
                            filter: filter
                        });
                    }
                } catch (e) {
                    this.logger.error(e);
                }
            }
        }

        return returnResult;
    }

    public createTest(version: string, dir: string): void {
        if (!shell.which('goss')) {
            return;
        }

        const testSourceDir = path.resolve(process.env.PWD as string, 'tests', version, dir),
            testTargetDir = path.resolve(process.env.PWD as string, version, dir)
        ;

        if (!fileExists(path.resolve(testSourceDir, 'tests.yaml'))) {
            this.logger.info(`File "${path.resolve(testSourceDir, 'tests.yaml')}" does not exists. Skipping generation.`);
            return;
        }

        shellExec(util.format(
            'goss -g "%s" render > "%s"',
            path.resolve(testSourceDir, 'tests.yaml'),
            path.resolve(testTargetDir, 'goss.yaml')
        ));

        if (!fileExists(path.resolve(testSourceDir, 'tests_wait.yaml'))) {
            this.logger.info(`File "${path.resolve(testSourceDir, 'tests_wait.yaml')}" does not exists. Skipping generation.`);
            return;
        }

        shellExec(util.format(
            'goss -g "%s" render > "%s"',
            path.resolve(testSourceDir, 'tests_wait.yaml'),
            path.resolve(testTargetDir, 'goss_wait.yaml')
        ));
    }

    public getVariantAliases(version: string, dir: string, semver: string, config: Configuration): string[] {
        const suite = path.dirname(dir),
            variant = path.basename(dir),
            alpineVersion = suite.replace(/^alpine/, ''),
            {
                defaultAlpineVersions = {},
                defaultDebianVersions = {},
                defaultAlpineSuite = App.DefaultAlpineSuite,
                defaultDebainSuite = App.DefaultDebianSuite,
                versionAliases: versionAliasesMap = {}
            } = config,
            versionAliases: string[] = [semver, version, ...((versionAliasesMap[version] ?? '').split(' '))].filter(v => v !== '');

        let variantAliases: string[] = [];
        if (variant != 'default') {
            variantAliases = [...versionAliases.map(v => v + '-' + variant)]
        }

        variantAliases = variantAliases.map(v => v.replace(/^latest-/, ''))
        if (variant == 'cli' || variant == 'default') {
            variantAliases.push(...versionAliases)
        }

        // append "-$suite" to every element
        const suiteAliases = variantAliases.map(v => v + '-' + suite)
        if (alpineVersion == (defaultAlpineVersions[version] ?? defaultAlpineSuite)) {
            // append "-alpine" to every element
            variantAliases = [...variantAliases, ...variantAliases.map(v => v + '-alpine')];
        } else if (alpineVersion != (defaultAlpineVersions[version] ?? defaultAlpineSuite)) {
            variantAliases = [];
        }

        variantAliases = [...suiteAliases, ...variantAliases];
        variantAliases = variantAliases.map(v => v.replace(/^latest-/, ''))
        // end Aliases

        return variantAliases;
    }

    public createVariant(version: string, dir: string, info: VersionBuildInfo): void {
        const suite = path.dirname(dir),
            variant = path.basename(dir),
            alpineVersion = suite.replace(/^alpine/, '')
        ;

        // alpine3.15 != 3.15, buster != buster
        const image = suite != alpineVersion ? {
                name: 'alpine',
                version: alpineVersion,
                variant: variant
            } : {
                name: 'debian',
                version: suite + '-slim',
                variant: variant
            },
            templateVars = {
                image: image,
                variables: info.variables,
                service: {
                    version: info.version,
                }
            };

        const files = fs.readdirSync((process.env.PWD as PathLike)).filter(file => /(.*\.template)/ig.test(file));
        for (const file of files) {
            const res = nunjucks.render(
                file,
                templateVars,
            );
            writeRenderedFile(path.resolve(version, dir, path.basename(file)), res)
        }
    }

    private projectContainsValidVersionsConfiguration(fileName: PathLike): ValidationResult {
        const jsonFile = parseJsonFile(fileName);
        const validator = new Validator(JsonSchema);
        const schemaValidationResult = validator.validate(jsonFile);

        if (!schemaValidationResult.valid) {
            return schemaValidationResult;
        }

        for (const [version] of Object.entries((jsonFile as Configuration).versions)) {
            if (semver.coerce(version) === null) {
                schemaValidationResult.errors.push({
                    instanceLocation: '#',
                    keyword: 'key',
                    keywordLocation: `#/properties/versions/additionalProperties`,
                    error: `Property "${version}" is not a valid version. See https://semver.org/ for help.`
                })
            }
        }

        return {
            valid: schemaValidationResult.errors.length == 0,
            errors: schemaValidationResult.errors
        };
    }

    private failWithLoggedJsonSchemaValidationErrors(
        filePath: PathLike,
        schemaValidationResult: ValidationResult
    ): false {
        this.logger.error(`${filePath} schema validation failed.`);
        schemaValidationResult.errors.forEach((outputUnit) => {
            this.logger.error(`There is an error located at ${outputUnit.instanceLocation}: ${outputUnit.error}`);
            this.logger.error(outputUnit)
        });

        return false;
    }
}
