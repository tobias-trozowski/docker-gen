export interface VersionBuildInfo {
    variants: string[],
    variables?: { [key: string]: any },
    version: string
}

export interface Configuration {
    defaultAlpineSuite?: string,
    defaultDebainSuite?: string,
    defaultAlpineVersions?: { [key: string]: string },
    defaultDebianVersions?: { [key: string]: string },
    versionAliases?: { [key: string]: string },
    versions: {
        [key: string]: VersionBuildInfo
    }
}
