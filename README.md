
# Docker Image Generator
Once i am happy with this package i'm gonna release it at my GitHub Account https://github.com/tobias-trozowski/


## Getting Started

First of all you have to clone the repository to your system:

```bash
$ git clone git@gitlab.com:tobias-trozowski/docker-gen.git
```

## Usage
Modify `./*/Dockerfile.jinja2` to your needs and run:
```
./generate.bash <DIRECTORY>
```
This will generate all versions and variants from the `versions.json` file and process the template `Dockerfile.jinja2`.

Example output:
```
$ ./generate.bash php
processing 8.1/alpine3.14/cli ...
processing 8.1/alpine3.14/fpm ...
```

After everything is generated you can run the build script to build your variant.
```
# Usage: build.bash <DIRECTORY> <IMAGE>
$ ./build.bash php/8.1/alpine3.14/cli php
```
This will build the image, tag it and run all tests on it automatically.

Some images which do not start a default service which runs continuously
need a little help to succeed the tests.
This is because the testing tool launches the image in a new container 
and run the tests on the newly started container.
Images which cannot run daemonized like `php-cli` or `docker-gen` will exit immediately. 

Just prepend `DGOSS_ARGS="tail -f /dev/null"` to the build command.
```
$ DGOSS_ARGS="tail -f /dev/null" ./build.bash php/8.1/alpine3.14/cli php
```
This will cause `dgoss` to use this specific command as image startup command 
and just keeps the container busy so the tests can run.
