# docker-gen/nginx

These image extends `nginx:1.18-alpine`.

## Environment variables
### Nginx environment variables
| Environment variable | Description | Default  |
| ------------- |:-------------:|:-----|
| `NGINX_USER` | TODO | `nginx` |
| `NGINX_WORKER_PROCESSES` | TODO | `auto` |
| `NGINX_WORKER_CONNECTIONS` | TODO | `768` |
| `NGINX_MULTI_ACCEPT` | TODO | `off` |
| `NGINX_SERVER_TOKENS` | TODO | `off` |
| `NGINX_CLIENT_MAX_BODY_SIZE` | TODO | `50m` |
| `NGINX_ENVSUBST_OUTPUT_DIR` | TODO | `/etc/nginx` |
| `NGINX_ENVSUBST_TEMPLATE_DIR` | TODO | `/usr/local/docker/etc/nginx/templates` |
| `NGINX_ACCESS_LOG` | TODO | `/var/log/nginx/access.log` |
| `NGINX_ERROR_LOG` | TODO | `/var/log/nginx/error.log` |

### Web environment variables
| Environment variable | Description | Default  |
| ------------- |:-------------:|:-----|
| `WEB_SERVER_NAME` | TODO | `*.vm` |
| `WEB_DOCUMENT_ROOT` | TODO | `/app/public` |
| `WEB_DOCUMENT_INDEX` | TODO | `index.html` |
