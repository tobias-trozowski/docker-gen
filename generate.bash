#!/usr/bin/env bash

set -Eeuo pipefail

CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/tobias-trozowski/docker-gen}

function usage() {
    >&2 cat <<-EOH
Usage: generate.bash <DIRECTORY>
EOH
    exit 1
}

if [ -z "${1:-}" ]; then
    echo "Missing directory" >&2
    usage
fi

if [ ! -d "${1:-}" ]; then
    echo "${1:-}: no such directory" >&2
    usage
fi

DIRECTORY="$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"
docker run --rm -u $(id -u):$(id -g) -v ${DIRECTORY}:/app -w /app -it ${CI_REGISTRY_IMAGE}/docker-gen
