#!/usr/bin/env bash
set -Eeuo pipefail

DOCKER_IMAGE_PATH=${1:-${DOCKER_IMAGE_PATH:-}}
DOCKER_IMAGE_NAME=${2:-${DOCKER_IMAGE_NAME:-}}
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/tobias-trozowski/docker-gen}
CI_REGISTRY_USER=${CI_REGISTRY_USER:-}
CI_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD:-}
NO_CACHE=${NO_CACHE:-1}
GOSS_FILES_PATH=${GOSS_FILES_PATH:-"${DOCKER_IMAGE_PATH}"}
DGOSS_ARGS=${DGOSS_ARGS:-}
CONTAINER_LOG_OUTPUT=${CONTAINER_LOG_OUTPUT:-"./dgoss.log"}
GOSS_FILES_STRATEGY=${GOSS_FILES_STRATEGY:-cp}

function usage() {
    >&2 cat <<-EOH
Usage: build.bash <DIRECTORY> <IMAGE>
EOH
    exit 1
}

if [ ! -d "${DOCKER_IMAGE_PATH:-}" ]; then
    echo "${DOCKER_IMAGE_PATH:-}: No such directory!" >&2
    usage
fi

if [ -z "$DOCKER_IMAGE_NAME" ]; then
    echo "Missing image name" >&2
    usage
fi

if [ ! -f "${DOCKER_IMAGE_PATH}/Dockerfile" ]; then
    echo "${DOCKER_IMAGE_PATH}/Dockerfile: No such file!" >&2
    exit 1
fi

if [ ! -f "${DOCKER_IMAGE_PATH}/tags.txt" ]; then
    echo "${DOCKER_IMAGE_PATH}/tags.txt: No such file!" >&2
    exit 1
fi


if [ ! -f "${GOSS_FILES_PATH}/goss.yaml" ] && [ -f "${GOSS_FILES_PATH}/tests/tests.yaml" ]; then
    echo "No goss.yaml found. Trying to generate new from ${DOCKER_IMAGE_PATH}/tests/tests.yaml"
    goss -g ${DOCKER_IMAGE_PATH}/tests/tests.yaml render > ${DOCKER_IMAGE_PATH}/goss.yaml
elif [ -f "${GOSS_FILES_PATH}/goss.yaml" ] && [ -f "${GOSS_FILES_PATH}/tests/tests.yaml" ]; then
    GOSS_MTIME=$(stat -c %Y "${GOSS_FILES_PATH}/goss.yaml")
    GOSS_SOURCE_MTIME=$(stat -c %Y "${GOSS_FILES_PATH}/tests/tests.yaml")
    if [ "$GOSS_SOURCE_MTIME" -gt "$GOSS_MTIME" ]; then
        echo "${GOSS_FILES_PATH}/tests/tests.yaml seems to be newer than ${GOSS_FILES_PATH}/goss.yaml. Regenerating..."
        goss -g ${DOCKER_IMAGE_PATH}/tests/tests.yaml render > ${DOCKER_IMAGE_PATH}/goss.yaml
    fi
fi

if [ ! -f "${GOSS_FILES_PATH}/goss_wait.yaml" ] && [ -f "${GOSS_FILES_PATH}/tests/tests_wait.yaml" ]; then
    echo "No goss_wait.yaml found. Trying to generate new from ${DOCKER_IMAGE_PATH}/tests/tests_wait.yaml"
    goss -g ${DOCKER_IMAGE_PATH}/tests/tests_wait.yaml render > ${DOCKER_IMAGE_PATH}/goss_wait.yaml
elif [ -f "${GOSS_FILES_PATH}/goss_wait.yaml" ] && [ -f "${GOSS_FILES_PATH}/tests/tests_wait.yaml" ]; then
    GOSS_WAIT_MTIME=$(stat -c %Y "${GOSS_FILES_PATH}/goss_wait.yaml")
    GOSS_WAIT_SOURCE_MTIME=$(stat -c %Y "${GOSS_FILES_PATH}/tests/tests_wait.yaml")
    if [ "$GOSS_WAIT_SOURCE_MTIME" -gt "$GOSS_WAIT_MTIME" ]; then
        echo "${GOSS_FILES_PATH}/tests/tests_wait.yaml seems to be newer than ${GOSS_FILES_PATH}/goss_wait.yaml. Regenerating..."
        goss -g ${DOCKER_IMAGE_PATH}/tests/tests_wait.yaml render > ${DOCKER_IMAGE_PATH}/goss_wait.yaml
    fi
fi

TAGS=($(<${DOCKER_IMAGE_PATH}/tags.txt))

if [ -z "$CI_REGISTRY_USER" ] && [ -z "$CI_REGISTRY_PASSWORD" ]; then
    echo "Local build detected, skipping docker login"
else
    echo ${CI_REGISTRY_PASSWORD} | docker login -u "${CI_REGISTRY_USER}" --password-stdin ${CI_REGISTRY}
fi

# pulling existing image and adjusting cache args when cache should be used
if [ -z "$NO_CACHE" ]; then docker pull ${CI_REGISTRY_IMAGE}/${DOCKER_IMAGE_NAME}:${TAGS} || true; fi
if [ ! -z "$NO_CACHE" ]; then export CACHE_ARGS="--no-cache"; else export CACHE_ARGS="--cache-from=${CI_REGISTRY_IMAGE}/${DOCKER_IMAGE_NAME}:${TAGS}"; fi

docker build --force-rm $CACHE_ARGS --compress ${TAGS[@]/#/-t ${CI_REGISTRY_IMAGE}/${DOCKER_IMAGE_NAME}:} ${DOCKER_IMAGE_PATH}

# running dgoss with verbose
export GOSS_FILES_PATH GOSS_FILES_STRATEGY
bash -x $(which dgoss) run $(docker images --format "{{.ID}}" | head -1) $DGOSS_ARGS || (cat $CONTAINER_LOG_OUTPUT && false)

docker image ls ${CI_REGISTRY_IMAGE}/${DOCKER_IMAGE_NAME}:*
